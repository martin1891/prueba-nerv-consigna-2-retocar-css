const { Router, request } = require('express');
const tickets = require('../sample.json');
const router = Router();


router.get('/', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.json(tickets);
});

router.post('/', (req, res) => {
    const { subject, date, from, status, body } = req.body;
    if( subject && date && from && body) {
        let ticketId = generarId(6);
        let status = "NEW"
        const newTicket = {...req.body, ticketId, status};
        tickets.push(newTicket);
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.json(tickets);
    } else {
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.send('Wrong Request');
    };
});

function generarId(num){
    const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let result1 = ' ';
    const charactersLength = characters.length;
    for(let i = 0; i < num; i++){
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result1;
};



module.exports = router;